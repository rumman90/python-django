def login_view(request):
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        login(request, user)
    return render(request, 'login.html', {'form': form})


# simple form usage in view
def my_view(request, template_name='home.html'):
    # sticks in a POST or renders an empty form
    form = MyForm(request.POST or None)
    if form.is_valid():
        do_something()
        return redirect('/')
    return render_to_response(template_name, {'form':form})

form = SignAlbumForm(Request.POST)
if form.is_valid():
    print form.save(commit=False).name
return render(Request, 'showname/index.html', {'form':form})


---------------------------------------------
class RecipeForm(ModelForm):
    class Meta:
        model=Recipe
fields=['title']

def create_recipe(request):
    if request.method == 'POST':
        form=RecipeForm(request.POST)
        if form.is_valid():
            title=form.cleaned_data['recipe_name']
            return HttpResponseRedirect('display.html')
        else:
            form=RecipeForm()
            return  render(request, 'create.html', {
        'form': form,
    })
----------------------------------------------