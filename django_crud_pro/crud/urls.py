
from django.contrib import admin
from django.urls import path

from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.home, name="index page"),
    path('addInfo/', views.addInfoContent, name="add page"),
    path('saveInfo/', views.saveInfoContent),
    path('editInfo/<id>', views.editBookInfo),
    path('updateInfo/<id>', views.updateInfoContent),
    path('deleteInfo/<id>', views.deleteBookInfo),
    #r'^(?P<id>\d+)/detail/$'
    url(r'^(?P<id>\d+)/$', views.Post_details, name='detail'), #regular expression tupi er por and dolar er age text bosano jay
]
