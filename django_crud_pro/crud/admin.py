from django.contrib import admin

# Register your models here.
from .models import booklist


class PostModelAdmin(admin.ModelAdmin):
    list_display = ["title", "price", "author"]  # admin e list akare dekhabe moder parametter pass
    list_display_links = ["price"]   # clickable link
    list_filter = ["author", "price"]  # default filter option anar jonno
    search_fields = ["title", "author"] #for search option
    #list_editable = ["title"]            #link editable

    class Meta:
        model = booklist

admin.site.register(booklist,PostModelAdmin)     #admin er sathe post class ke register kore dilam