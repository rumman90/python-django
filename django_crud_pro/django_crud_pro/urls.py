
from django.contrib import admin
from django.urls import path

from crud import views
from django.urls.conf import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("crud.urls")),
]
