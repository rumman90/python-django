from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib import messages

from .models import booklist
#for pagination
from django.core.paginator import Paginator

# def post_home(response):
#     return HttpResponse("<h1> rumman </h1>")  #function dara view

#---------------------------------select-----------------------------------------
def home(request):
    #data select from database
    # query = booklist.objects.all().order_by("-title"); #- bolte reverse bujhay
    query = booklist.objects.all();

    #for pagination
    paginator = Paginator(query, 3)  # Show 3 contacts per page
    page =  request.GET.get('page')
    querylist =  paginator.get_page(page)

    context = {
        "title": "python",
        "content": "Select Data From Database",
        #"querySet": query,
        "querySet": querylist
    }
    return render(request, 'index.html', context)
    #return render(request, 'index.html', {"querySet":query})



#---------------------------------add-----------------------------------------
def addInfoContent(request):
    context = {
        "title": "add",
    }
    return render(request, 'addinfo.html', context)

def saveInfoContent(request):
    #print(request.POST)
    #print(request.POST.get('title'))

    #for get method
    # titlePost = request.GET['title']
    # pricePost = request.GET['price']
    # authorPost = request.GET['author']
    # save_info_details = booklist(title=titlePost, price=pricePost, author=authorPost)
    # save_info_details.save()
    #return redirect('/')

    save_info_details = booklist()
    save_info_details.title = request.POST['title']
    save_info_details.price = request.POST['price']
    save_info_details.author = request.POST['author']
    save_info_details.image = request.FILES['image']
    #save_info_details.author = request.POST.get('author')
    save_info_details.save()
    messages.success(request,"Successfully Save Information ")
    return redirect('/')



#---------------------------------delete-----------------------------------------
def deleteBookInfo(request,id):
    #pass
    recInfobyId=booklist.objects.get(pk=id);
    recInfobyId.delete()
    messages.warning(request, 'Delete Information')
    return redirect('/')

#---------------------------------edit-----------------------------------------
def editBookInfo(request, id):
    recInfobyId = booklist.objects.get(pk=id)
    context = {
        'recEditInfo': recInfobyId,
        "titel": "Edit page",
    }
    return render(request, 'editInfo.html', context)


def updateInfoContent(request, id):
    recInfobyId = booklist.objects.get(pk=id)
    recInfobyId.title = request.GET['title']
    recInfobyId.price = request.GET['price']
    recInfobyId.author = request.GET['author']
    recInfobyId.save()
    return redirect('/')
#---------------------------------end-----------------------------------------

# =====================================================================================

def Post_details(request, id):
    instence = get_object_or_404(booklist, id=id);   #model
    context = {
        "titel": "details page here",
        "content": "Information Details",
        "instence": instence
    }
    return render(request, 'details.html', context)